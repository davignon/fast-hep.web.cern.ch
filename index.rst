Welcome to the Faster Analysis Software Taskforce (FAST) webpage!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   how-tos.rst
